<?php
$deviceName = '192.168.56.101:5555';

if (count($argv) <= 1)
{
    echo 'argv enter. isAuto dungeon ip ex) 1 2-14 101';
    exit;
}

$isAuto = ($argv[1] == '1') ? true : false;
list($area, $stage) = explode('-', $argv[2]);
$deviceIp = (isset($argv[3]) && $argv[3] != '') ? $argv[3] : '101';
$deviceName = "192.168.56.{$deviceIp}:5555";
echo 'deviceIp = ' . $deviceIp . PHP_EOL;

// 버튼이 클릭이 안될 때 재시작하기 위한 카운트.
$isFirst = true;
$stopCount = 0;

// dungeon, infinity
$mode = 'dungeon';
//$mode = 'infinity';
$isReplay = false;
$isDungeonStart = false;
$isInfinityStart = false;

while(true)
{
    echo 'mode = ' . $mode . ', isAuto = ' . $isAuto . ', area = ' . $area . ', stage = ' . $stage . PHP_EOL;
    if ($isFirst)
    {
        // 게임 시작.
        echo 'start dragon...' . PHP_EOL;
        exec("adb -s {$deviceName} shell am start -n com.SnowPopcorn.EnDragon/com.SnowPopcorn.EnDragon.MainActivity");

        $isFirst = false;
        $stopCount = 0;
    }
    // screen capture
    echo "screen capture..." . PHP_EOL;
    system("adb -s {$deviceName} shell screencap -p | perl -pe 's/\\x0D\\x0A/\\x0A/g' > screen{$deviceIp}.png");
    $img = imagecreatefrompng("screen{$deviceIp}.png");

    checkClick($img, 41, 732, 16514043, '광고 닫기');
    checkClick($img, 401, 150, 15512850, '화면을 터치해주세요.');
    checkClick($img, 47, 369, 6819340, '출석 확인');
    // 자동인 경우 켜기.
    if ($isAuto == false)
    {
        if (checkPixel($img, 456, 774, 14207669, '게임화면인 경우 자동전투 클릭'))
        {
            sleep(3);
            clickEvent(310, 762);
            $isAuto = true;
        }
    }

    if ($mode == 'dungeon')
    {
        checkSpecifyClick($img, 411, 341, 16499305, 78, 500, '무한의 탑 결과화면 로비로 이동');
        checkSpecifyClick($img, 398, 89, 9214616, 413, 43, '무한의 탑 뒤로가기');
        checkClick($img, 44, 470, 8485993, '던전 입장');
        $area1 = checkPixel($img, 387, 250, 5987670, 'area1 check');
        $area2 = checkPixel($img, 385, 159, 2831638, 'area2 check');
        $area3 = checkPixel($img, 334, 109, 3619116, 'area3 check');
        $area4 = checkPixel($img, 387, 250, 3659499, 'area4 check');
        if ($area1 && $area == '1')
        {
            switch($stage)
            {
                case '5':
                     clickEvent(140, 267); break;
                case '6':
                     clickEvent(138, 139); break;
                case '7':
                     clickEvent(230, 184); break;
                case '8':
                    clickEvent(312, 198); break;
                case '9':
                    clickEvent(216, 267); break;
                case '10':
                    clickEvent(202, 391); break;
                case '11':
                    clickEvent(222, 498); break;
                case '12':
                    clickEvent(275, 564); break;
                case '13':
                    clickEvent(351, 518); break;
                case '14':
                    clickEvent(328, 426); break;
            }
        }
        else if ($area2 && $area == '2')
        {
            switch($stage)
            {
                case '1':
                    clickEvent(53, 481); break;
                case '2':
                    clickEvent(95, 569); break;
                case '3':
                    clickEvent(181, 595); break;
                case '4':
                    clickEvent(243, 678); break;
                case '5':
                    clickEvent(320, 620); break;
                case '6':
                    clickEvent(255, 535); break;
                case '7':
                    clickEvent(188, 472); break;
                case '8':
                    clickEvent(153, 358); break;
                case '9':
                    clickEvent(99, 251); break;
                case '10':
                    clickEvent(177, 200); break;
                case '11':
                    clickEvent(212, 308); break;
                case '12':
                    clickEvent(230, 400); break;
                case '13':
                    clickEvent(312, 404); break;
                case '14':
                    clickEvent(292, 302); break;
                case '15':
                    clickEvent(270, 174); break;
            }
        }
        else if ($area3 && $area == '3')
        {
            switch($stage)
            {
                case '1':
                    clickEvent(50, 400); break;
                case '2':
                    clickEvent(59, 306); break;
                case '3':
                    clickEvent(70, 200); break;
                case '4':
                    clickEvent(256, 201); break;
                case '5':
                    clickEvent(260, 204); break;
                case '6':
                    clickEvent(332, 197); break;
                case '7':
                    clickEvent(288, 295); break;
                case '8':
                    clickEvent(242, 377); break;
                case '9':
                    clickEvent(307, 472); break;
                case '10':
                    clickEvent(253, 534); break;
                case '11':
                    clickEvent(338, 619); break;
                case '12':
                    clickEvent(307, 719); break;
                case '13':
                    clickEvent(222, 650); break;
                case '14':
                    clickEvent(147, 530); break;
                case '15':
                    clickEvent(67, 641); break;
            }
        }
        else if ($area4 && $area == '4')
        {
            switch($stage)
            {
                case '1':
                    clickEvent(366, 416); break;
                case '2':
                    clickEvent(347, 520); break;
                case '3':
                    clickEvent(294, 568); break;
                case '4':
                    clickEvent(302, 456); break;
                case '5':
                    clickEvent(279, 357); break;
                case '6':
                    clickEvent(303, 234); break;
                case '7':
                    clickEvent(266, 144); break;
                case '8':
                    clickEvent(174, 264); break;
                case '9':
                    clickEvent(80, 303); break;
                case '10':
                    clickEvent(178, 395); break;
                case '11':
                    clickEvent(211, 475); break;
                case '12':
                    clickEvent(219, 596); break;
                case '13':
                    clickEvent(144, 529); break;
                case '14':
                    clickEvent(64, 459); break;
                case '15':
                    clickEvent(40, 605); break;
            }
        }
        // 맵페이지에 있는데 선택한 스테이지가 아닌경우 스테이지 넘김.
        else if ($area1 || $area2 || $area3 || $area4)
        {
            echo '다음맵->' . PHP_EOL;
            clickEvent(240, 746);
        }

        checkClick($img, 101, 277, 16708864, '혼자하기');
        $isDungeonStart = checkClick($img, 45, 684, 7431011, '던전 게임시작');
        $isReplay = checkClick($img, 47, 430, 12407345, '다시하기');
        checkSpecifyClick($img, 294, 375, 16711680, 161, 476, '인벤토리가 부족할 때 게임시작');
        $notExistFire1 = checkSpecifyClick($img, 254, 366, 16711680, 160, 397, '횟불이 부족합니다 확인');
        $notExistFire2 = checkSpecifyClick($img, 285, 347, 16711680, 169, 306, '횟불이 부족할 때 취소');
        if ($notExistFire1 || $notExistFire2)
        {
            $mode = 'infinity';
        }
    }
    else if ($mode == 'infinity')
    {
        checkClick($img, 39, 702, 2365716, '던전결과화면 나가기');
        #checkSpecifyClick($img, 45, 684, 14620634, 410, 39, '던전 게임시작 뒤로가기');
        checkSpecifyClick($img, 45, 684, 7431011, 410, 39, '던전 게임시작 뒤로가기');
        $area1 = checkPixel($img, 387, 250, 5987670, 'area1 check');
        $area2 = checkPixel($img, 385, 159, 2831638, 'area2 check');
        $area3 = checkPixel($img, 334, 109, 3619116, 'area3 check');
        $area4 = checkPixel($img, 387, 250, 3659499, 'area4 check');
        if ($area1 || $area2 || $area3 || $area4)
        {
            echo '스테이지 선택 뒤로가기' . PHP_EOL;
            clickEvent(412, 42);
        }
        checkSpecifyClick($img, 347, 123, 6843221, 410, 39, '던전입장 뒤로가기');
        checkClick($img, 39, 709, 6774851, '무한의 탑 입장');
        $isInfinityStart = checkClick($img, 67, 651, 13245906, '무한의 탑 게임시작');
        //checkClick($img, 83, 363, 2431252, '확인');
        $isReplay = checkClick($img, 70, 292, 5185563, '다시하기');
        $notExistNapal1 = checkSpecifyClick($img, 262, 366, 16711680, 160, 397, '전장나팔이 부족합니다 확인');
        $notExistNapal2 = checkClick($img, 175, 311, 6381135, '전장나팔이 부족할 때 취소');
        if ($notExistNapal1 || $notExistNapal2)
        {
            $mode = 'dungeon';
        }
    }

    // 다시하기가 클릭되지 않거나 열쇠가 없는 경우 카운트. 300카운트면 재시작.
    if (!$isReplay && !$isInfinityStart && !$isDungeonStart)
    {
        $stopCount++;
        if ($stopCount == 300)
        {
            echo 'stop!!! kill dragon...';
            exec("adb -s {$deviceName} shell am force-stop com.SnowPopcorn.EnDragon");
            sleep(10);
            $isFirst = true;
            $isReplay = false;
            $isDungeonStart = false;
            $isInfinityStart = false;
            $isAuto = false;
        }
    }
    else
    {
        $isReplay = false;
        $isDungeonStart = false;
        $isInfinityStart = false;
        $stopCount = 0;
    }
    echo 'stopCount = ' . $stopCount . PHP_EOL;
    echo 'sleep 2 second...' . PHP_EOL;
    sleep(2);
}

function checkClick($img, $x, $y, $color, $ment = '')
{
    $result = checkPixel($img, $x, $y, $color, $ment);
    if ($result)
    {
        clickEvent($x, $y);
        echo $ment . ' click!' . PHP_EOL;
    }
    return $result;
}

function checkTwoSpecifyClick($img, $x1, $y1, $color1, $x2, $y2, $color2, $clickX, $clickY, $ment)
{
    $result1 = checkPixel($img, $x1, $y1, $color1, $ment);
    $result2 = checkPixel($img, $x2, $y2, $color2, $ment);
    if ($result1 && $result2)
    {
        echo "touch {$ment}!" . PHP_EOL;
        clickEvent($clickX, $clickY);
    }
    return $result1 && $result2;
}

function checkSpecifyClick($img, $x, $y, $color, $clickX, $clickY, $ment)
{
    $result = checkPixel($img, $x, $y, $color, $ment);
    if ($result)
    {
        clickEvent($clickX, $clickY);
        echo $ment . ' specify click!' . PHP_EOL;
    }
    return $result;
}

function checkPixel($img, $x, $y, $color, $ment)
{
    $rgb = imagecolorat($img, $x, $y);
    var_dump($ment . ' = ' . $rgb);
    if ($rgb == $color)
    {
        return true;
    }
    return false;
}

function clickEvent($x, $y, $event = 'event7')
{
    global $deviceName;
    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x1, 0x14a, 0x1);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x1);
    $exec .= sendEvent($event, 0x3, 0x35, $x);
    $exec .= sendEvent($event, 0x3, 0x36, $y);
    $exec .= sendEvent($event, 0x0, 0x2, 0x0);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);

    $exec .= sendEvent($event, 0x1, 0x14a, 0x0);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x0);
    $exec .= sendEvent($event, 0x3, 0x35, $x);
    $exec .= sendEvent($event, 0x3, 0x36, $y);
    $exec .= sendEvent($event, 0x0, 0x2, 0x0);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= "\"";
    exec($exec);
}

function drag($startX, $startY, $endX, $endY, $event = 'event7')
{
    global $deviceName;

    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x1, 0x14a, 0x1);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x1);
    $exec .= "\"";
    exec($exec);
    $splitValue = 5;

    $intervalValueX = ($startX - $endX) / $splitValue;
    $intervalValueY = ($startY - $endY) / $splitValue;

    for ($i = 0; $i < $splitValue; $i++)
    {
        $exec = "adb -s {$deviceName} shell \"";
        $exec .= sendEvent($event, 0x3, 0x35, $startX - ($i * $intervalValueX));
        $exec .= sendEvent($event, 0x3, 0x36, $startY - ($i * $intervalValueY));
        $exec .= sendEvent($event, 0x0, 0x2, 0x0);
        $exec .= sendEvent($event, 0x0, 0x0, 0x0);
        $exec .= "\"";
        exec($exec);
    }
    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x1, 0x14a, 0x0);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x0);
    $exec .= sendEvent($event, 0x3, 0x35, $endX);
    $exec .= sendEvent($event, 0x3, 0x36, $endY);
    $exec .= sendEvent($event, 0x0, 0x2, 0x0);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= "\"";
    exec($exec);
    echo 'drag ' . $startX . ', ' . $startY . ', ' . $endX . ', ' . $endY . PHP_EOL;
}

function sendEvent($event, $one, $two, $three)
{
    return "sendevent /dev/input/{$event} {$one} {$two} {$three};";
}
